from . import gp_func190612 as func
from .definition import *
from collections import OrderedDict
import re

gtypes = ["N", "O", "L", "G", "none"]
# start = time.time()
#
# # func.read_enzyme("./HEK/curated/HEK_test_enzyme.txt")
# func.read_enzyme("/Users/sachiko/Dropbox/Simulation/Ver9/Ver9-2/dme_enzyme.txt")
# func.read_glystr("/Users/sachiko/Dropbox/Simulation/Ver9/glystr_newco.csv")
# print(func.enzymes)
# print(func.glystr)
#
# max = 12
# # rea = 0
#
# # def count_size(LC: str):
# #     unit = re.compile('[A-Z]{1,2}[a|b][1-6]?')
# #     size = len(unit.findall(LC))
# #     return size


def reaction(parent_str: str, sub_str: re, pro_str: re, start: int) -> str:
    child_str = (parent_str[:start] + sub_str.sub(pro_str, parent_str[start:], 1)).lstrip("(")
    return child_str


def judge(parent: func.Glycan, enzyme: func.Enzyme, gtype: str, max):
    parent_str = "(" + parent.structure
    childs = []
    # sub_str = re.compile(enzyme.substrate)
    # print(sub_str)
    # print(enzyme.substrate)

    # complile substrate string
    cp_sub = re.compile(enzyme.substrate)
    match = cp_sub.finditer(parent_str)

    # global rea
    # rea += len(enzyme.substrate.findall(parent_str))
    for m in match:
        con_flag = 0
        print(m.group(), enzyme.name)

        for constraint in enzyme.constraint:
            if constraint == '':
                con_flag += 1
            # *の中に何がはいるかの条件constraint
            # elif "SpaceIs" in constraint:
            #     Space = constraint.split("\\1")[1]
            #     if m.group('space') == Space:
            #         con_flag += len(enzyme.constraint)
            # elif "SpaceIsNot" in constraint:
            #     Space = constraint.split("\\1")[1]
            #     if m.group('space') != Space:
            #         con_flag += len(enzyme.constraint)
            # elif "NotOn" in constraint:
            #     Not_re = re.compile(constraint.split("\\1")[1])
            #     if not Not_re.match(m.string[m.end():]):
            #         con_flag += 1
            else:
                Have_re = re.compile(constraint)
                # nの条件式が成り立つ時、constrintを".*"に書き換えて正規表現で通るようにする
                if "n" in constraint:
                    code_n = re.finditer(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",constraint)
                    for c_n in code_n:
                        # constrinの個数条件
                        n_count = re.search(r"\d", c_n.group())
                        # constrintの個数を制限する文字
                        n_line = re.search(r"[A-Z]+[a|b]?[1-8]?", c_n.group())
                        # 親糖鎖のnで制限された文字のカウント
                        n_parent = parent_str.count(n_line.group())
                        if re.search(r"\\[<>=](\\[<>=])?", c_n.group()).group() == "\\>\\=":
                            if int(n_parent) >= int(n_count.group()):
                                # nの条件式を".*"に書き換えてコンパイルする
                                Have_re = re.compile(re.sub(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",".*",constraint,1))
                        elif re.search(r"\\[<>=](\\[<>=])?", c_n.group()).group() == "\\<\\=":
                            if int(n_parent) <= int(n_count.group()):
                                Have_re = re.compile(re.sub(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",".*",constraint,1))
                        elif re.search(r"\\[<>=](\\[<>=])?", c_n.group()).group() == "\\=":
                            if int(n_parent) == int(n_count.group()):
                                Have_re = re.compile(re.sub(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",".*",constraint,1))
                        elif re.search(r"\\[<>=](\\[<>=])?", c_n.group()).group() == "\\>":
                            if int(n_parent) > int(n_count.group()):
                                Have_re = re.compile(re.sub(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",".*",constraint,1))
                        elif re.search(r"\\[<>=](\\[<>=])?", c_n.group()).group() == "\\<":
                            if int(n_parent) < int(n_count.group()):
                                Have_re = re.compile(re.sub(r"n[A-Z]*[a|b]?[1-8]?\\[<>=](\\[<>=])?\d",".*",constraint,1))
                # リアクションサイトの条件
                if "@" in constraint:
                    posi_count = 0
                    # parent_strのenzymのsubstrateと一致する範囲に@をいれて一致する場合con_flag+=1する
                    while True:
                        # リアクションサイト候補
                        reac_parent_str = parent_str[:m.start()+posi_count]+"@"+parent_str[m.start()+posi_count:]
                        if Have_re.search(reac_parent_str):
                            con_flag += 1
                            break
                        elif posi_count is m.end():
                            break
                        else:
                            posi_count += 1
                elif Have_re.search(parent_str):
                    con_flag += 1


        if con_flag == len(enzyme.constraint):
            child_str = reaction(parent_str, cp_sub, enzyme.product, m.start())

            if enzyme.Class == "GH":
                child_size = parent.size - 1
            elif enzyme.Class == "GT":
                child_size = parent.size + 1
            else:
                if ";" in child_str:
                    child_size = len(func.gly.from_LC(child_str.split(";")[0]))
                elif ":" in child_str:
                    child_size = len(func.gly.from_LC(child_str.split(":")[0]))
                elif "#" in child_str:
                    child_size = len(func.gly.from_LC(child_str.split("#")[0]))
                else:
                    child_size = len(func.gly.from_LC(child_str))

            if child_size <= max:
                try:
                    childs.append(func.Glycan(child_str, child_size, gtype))
                except func.gly.PositionError:
                    pass

    return childs
       
def reverse_judge(parent: func.Glycan, enzyme: func.Enzyme, gtype: str, mMax: int):
    parent_str = "(" + parent.structure
    childs = []
    # sub_str = re.compile(enzyme.substrate)
    # print(sub_str)
    # print(enzyme.substrate)
    
    # compile product string

    cp_pro = re.compile(enzyme.product)
    match = cp_pro.finditer(parent_str)

    # global rea
    # rea += len(enzyme.substrate.findall(parent_str))
    for m in match:
        child_str = reaction(parent_str, cp_pro, enzyme.substrate, m.start())
        if enzyme.Class == "GH":
            child_size = parent.size + 1
        elif enzyme.Class == "GT":
            child_size = parent.size - 1
        else:
            if ";" in child_str:
                child_size = len(func.gly.from_LC(child_str.split(";")[0]))
            elif ":" in child_str:
                child_size = len(func.gly.from_LC(child_str.split(":")[0]))
            elif "#" in child_str:
                child_size = len(func.gly.from_LC(child_str.split("#")[0]))
            else:
                child_size = len(func.gly.from_LC(child_str))

        if child_size <= mMax:
            try:
                child = func.Glycan(child_str, child_size, gtype)

            except func.gly.PositionError:
                child = None
            
            if child is not None:

                con_flag = 0
                print(enzyme.name, child.structure)

                for constraint in enzyme.constraint:
                    if constraint == '':
                        con_flag += 1
                    
                    else:
                        Have_re = re.compile(constraint)
                        # 単一？
                        if "@" in constraint:

                            posi_count = 0
                            while True:
                                # リアクションサイト候補
                                reac_parent_str = parent_str[:m.start()+posi_count]+"@"+parent_str[m.start()+posi_count:]
                                if Have_re.search(reac_parent_str):
                                    con_flag += 1
                                    break
                                elif posi_count is m.end():
                                    break
                                else:
                                    posi_count += 1
                        elif Have_re.search(child.structure):
                            con_flag += 1


                if con_flag == len(enzyme.constraint):
                    childs.append(child)
                else:
                    del child
    return childs

def main(specificity, init_glycan, mMax: int, glystr_file="", glytree_file=""):
    """ Glycan Pathway Predicter (GPP) function 
    Parameters:
        string: specificity  --- the text describes enzyme specificity 
        string: init_glycan  --- the text describes initial glycans 
        int:    mMax          --- the maximum number of oligosaccharides GPP will predict
        string: glystr_file  --- file name
        string: glytree_file --- file name
    
    Returns:
        three variables 
        glystr  --- glycan linearCode Information (e.g. index, LC, numOfOligos, glycanType)
        glytree --- reaction information between glycans 
        (e.g. index, child_glycan_index, parent_glycan_index, donor, product, type of Enzyme)
        glystr, glytree, enzymes
    """

    # list of glycan types N type O type Lipid G? 
    gtypes = ["N", "O", "L", "G", "none"]
    # allocate dictionary memory 
    glystr, glytree, enzymes = func.init_dict(gtypes)

    try:
        # read data
        specificities = init_file(specificity)
        func.read_enzyme(enzymes, specificities)
    except ValueError:
        print("Argument about spcificity is not string or list.")
        return

    try:
        init_glycans = init_file(init_glycan)
        func.read_glystr(glystr, init_glycans)
    except ValueError:
        print("Argument about init_glycan is not string or list.")
        return

    try:
        mMax = int(mMax)
    except ValueError:
        print("Argument about max is not integer.")
        return


    # construct reaction pathway 
    for gtype in gtypes:
        i = 0
        # print(gtype)
        while i < len(glystr[gtype]):
            parent = list(glystr[gtype].values())[i]
            for enzyme in enzymes[gtype] + enzymes["none"]:
                # predict product reactions
                childs = judge(parent, enzyme, gtype, mMax)
                if len(childs) > 0:
                    for child in childs:
                        child = func.store_glystr(glystr, child)
                        # the reaction has not already been registered 
                        index = gtype+ 'R' +str(len(glytree[gtype])+1)
                        
                        rea = Reaction()
                        rea.set_glycan(index, child, parent, enzyme)
                        func.store_glytree(glytree, rea)
                       

            # Count Gtypes pathways e.g. N: 0, O: 9 ...
            i += 1

            print(i, type(glystr[gtype]))

    if glystr_file != "":
        func.write_glystr(glystr, glystr_file)
    if glytree_file != "":
        func.write_glytree(glytree, glytree_file)

    return glystr, glytree, enzymes

def fwd_pred_with_glycan_profile(specificity, init_glycan, glycan_profile, mMax: int, glystr_file="", glytree_file=""):
    glystr, glytree, enzymes = main(specificity, init_glycan, mMax, glystr_file="", glytree_file="")

    new_glystr, new_glytree, _ = func.init_dict(gtypes)

    for gtype in gtypes:
        for i in range(len(glycan_profile)):
            LC = glycan_profile[i]["name"]
            LC_type = glycan_profile[i]["gtype"]
            

            if  LC_type != gtype:
                continue
            print("find_rxns_from_init_glycan")
            rxns, glycans = find_rxns_from_LC(LC, glytree[gtype], "forward")

            for glycan in glycans:

                func.store_glystr(new_glystr, glycan)

            for rxn in rxns:

                func.store_glytree(new_glytree, rxn)

    if glystr_file != "":
        func.write_glystr(new_glystr, glystr_file)
    if glytree_file != "":
        func.write_glytree(new_glytree, glytree_file)
    
    return new_glystr, new_glytree, enzymes

def reverse_prediction(specificity, glycan_profile, mMax: int, glystr_file="", glytree_file=""):
    """ This function predicts patyways from glycan profile.
        @input
            specificity --- enzyme specificity file (string)
            init_glycan --- initiation glycan information (string)
            glycan_profile --- glycan (string)
            mMax --- the maximum number of Monosaccharides
    """
    # list of glycan types N type O type Lipid G? 
    
    # allocate dictionary memory 
    glystr, glytree, enzymes = func.init_dict(gtypes)
    
    try:
        # read data
        specificities = init_file(specificity)
        func.reverse_read_enzyme(enzymes, specificities)
    except ValueError:
        print("Argument about spcificity is not string or list.")
        return
   
    try:
        
        func.read_glycan_profile(glystr, glycan_profile)
    except ValueError:
        print("Argument about glycan_profile is not string or list.")
        return

    try:
        mMax = int(mMax)
    except ValueError:
        print("Argument about max is not integer.")
        return

    # construct reaction pathway 
    for gtype in gtypes:
        i = 0
        # print(gtype)
        while i < len(glystr[gtype]):
            # parent will be Product
            parent = list(glystr[gtype].values())[i]
            for enzyme in enzymes[gtype] + enzymes["none"]:
                # predict product reactions
                # childs will be Substrate 
                childs = reverse_judge(parent, enzyme, gtype, mMax)
                if len(childs) > 0:
                    for child in childs:
                        child = func.store_glystr(glystr, child)
                        # the reaction has not already been registered 
                      
                        index = gtype+ 'R' +str(len(glytree[gtype])+1)
                        
                        rea = Reaction()
                        # flip the relation ship parent child 
                        rea.set_glycan(index, parent, child, enzyme)
                        func.store_glytree(glytree, rea)
            # Count Gtypes pathways e.g. N: 0, O: 9 ...
            i += 1

            #print(i, glystr[gtype])
            print(i)


    if glystr_file != "":
        func.write_glystr(glystr, glystr_file)
    if glytree_file != "":
        func.write_glytree(glytree, glytree_file)
        
    return glystr, glytree, enzymes

def reverse_prediction_with_glycan_profile(specificity, init_glycan, glycan_profile, mMax: int, glystr_file="", glytree_file=""):
    """ This function predicts patyways from glycan profile.
        @input
            specificity --- enzyme specificity file (string)
            init_glycan --- initiation glycan information (string)
            glycan_profile --- glycan (string)
            mMax --- the maximum number of Monosaccharides
    """

    # list of glycan types N type O type Lipid G? 
    gtypes = ["N", "O", "L", "G", "none"]
    # allocate dictionary memory 
    reverse_glystr, reverse_glytree, enzymes = reverse_prediction(specificity, glycan_profile, mMax)
    glystr, glytree, _ = func.init_dict(gtypes)

    try:
        init_glycans = init_file(init_glycan)
        #func.read_glystr(glystr, init_glycans)
        
    except ValueError:
        print("Argument about init_glycan is not string or list.")
        return
    
    # find path from init glycan
    for gtype in gtypes:
        # glycans
        for i in range(len(init_glycans)):
            # take glystr
            init_glystr = init_glycans[i].split(",")[1]
            init_gtype = init_glycans[i].rsplit()[0].split(",")[3]
            
            if init_gtype != gtype:
                continue
            print("find_rxns_from_init_glycan")
            init_rxns, glycans = find_rxns_from_LC(init_glystr, reverse_glytree[gtype])

            for glycan in glycans:

                func.store_glystr(glystr, glycan)

            for rxn in init_rxns:

                func.store_glytree(glytree, rxn)

    if glystr_file != "":
        func.write_glystr(glystr, glystr_file)
    if glytree_file != "":
        func.write_glytree(glytree, glytree_file)
        
    return glystr, glytree, enzymes

def find_rxns_from_LC(LC: str, glytree: OrderedDict, direction="backward"):
    """ helper function of reverse_prediction. """

    # rxns_set returns Reaction objct list that has pruned network.
    rxns_set = set()
    glycans = set()
    i = 0
    
    if direction == "backward":

        # get rxns glycans which includes LC
        for key, rxn in glytree.items():
        # if child_glycan 
            
            if rxn.parent.structure == LC:
                i += 1
                print("glytree iteration ", i)
                # add initial glycans 
                
                rxns_set.add(rxn)

                glycans.add(rxn.child)
                glycans.add(rxn.parent)
                
        rxns_set = list(rxns_set)

        # get rxns and glycans from LC
        for tmp in rxns_set:
            
            i = 0
            #print("rxns_set : 0")
            for rxn in glytree.values():
            
                # if child_glycan 
                if rxn.parent.structure == tmp.child.structure and not rxn in rxns_set:
                    i += 1
                    print("rxns", i)
                    #add initial glycans 
                    rxns_set.append(rxn)
                    
                    glycans.add(rxn.child)
                    glycans.add(rxn.parent)

    if direction == "forward":
        # get rxns glycans which includes LC
        for key, rxn in glytree.items():
        # if child_glycan 
            
            if rxn.child.structure == LC:
                i += 1
                print("glytree iteration ", i)
                # add initial glycans 
                
                rxns_set.add(rxn)

                glycans.add(rxn.child)
                glycans.add(rxn.parent)
                
        rxns_set = list(rxns_set)

        # get rxns and glycans from LC
        for tmp in rxns_set:
            
            i = 0
            #print("rxns_set : 0")
            for rxn in glytree.values():
            
                # if child_glycan 
                if rxn.child.structure == tmp.parent.structure and not rxn in rxns_set:
                    i += 1
                    print("rxns", i)
                    #add initial glycans 
                    rxns_set.append(rxn)
                    
                    glycans.add(rxn.child)
                    glycans.add(rxn.parent)
        rxns_set = set(rxns_set)
        
        
    return rxns_set, glycans




def find_potential_glycans(glystr, glyprofs):
    """
        Using glystr file, We predict potential glycans
        in gly_profs.
        gly_profs will include uncertain LC code
        such as 
        Ab?ANa;S    <- uncertain linkage,
        A??G        <- uncertain linkage and anomer,
        *G          <- Unknown sacccharide
        ANb3*A      <- Unknown 3 saccharides
        Ab3/4       <- linkage is -3 or -4
        Ab3//GNb3AN <- possible SU is Ab3 or GNb3 

        returns:
            json 

    """

    potentialLC = {
        "N": {},
        "O": {},
        "L": {},
        "G": {},
        "none": {}

    }
    for gtype in gtypes:
        for profLC in glyprofs[gtype]:
            regLC = profLC
            reg = re.compile(func.glycan_set_regex(regLC))
            potentialLC[gtype][profLC] = []
            for gly in glystr[gtype]:
                potLC = gly

                if reg.match(potLC):
                    potentialLC[gtype][profLC].append(potLC)
    
    return potentialLC
    

def check_LC(LC: str):
    """check the string follows the LC format."""
    reg = re.compile(r"([A-Z]{1,2}(\[.+\])?[a|b][1-8])+")

    if reg.match(LC) != None:
        return True
    return False



# for gtype in func.gtypes:
#     i = 0
#     print(gtype)
#     # for parent in func.glystr[gtype]:
#     #     print(func.glystr[gtype][parent].structure)
#     #     for enzyme in func.enzymes["none"]:
#     #         # print("in", enzyme.name)
#     #         childs = judge(func.glystr[gtype][parent], enzyme, gtype)
#     #         if len(childs) > 0:
#     #             for child in childs:
#     #                 child = func.store_glystr(child)
#     #                 glytree[gtype].append([child.id, func.glystr[gtype][parent].id, enzyme.name])
#     while i < len(func.glystr[gtype]):
#         parent = list(func.glystr[gtype].values())[i]
#         # print(parent.structure)
#         for enzyme in func.enzymes[gtype] + func.enzymes["none"]:
#             childs = judge(parent, enzyme, gtype)
#             if len(childs) > 0:
#                 for child in childs:
#                     child = func.store_glystr(child)
#                     if not [child.id, parent.id, enzyme.name, enzyme.cosubstrate, enzyme.Class] in func.glytree[gtype]:
#                         func.glytree[gtype].append([child.id, parent.id, enzyme.name, enzyme.cosubstrate, enzyme.Class])
#         i += 1
#         print(i, len(func.glystr[gtype]))
#
#
# print(func.glytree)
# # print(func.glystr)
# func.write_glystr("/Users/sachiko/Dropbox/Simulation/Ver9/Ver9-2/glystr_all.csv")
# func.write_glytree("/Users/sachiko/Dropbox/Simulation/Ver9/Ver9-2/glytree_all.csv")
# # print(rea)
#
# elapsed_time = time.time() - start
# print("elapsed_time:{0}".format(elapsed_time) + "[sec]")