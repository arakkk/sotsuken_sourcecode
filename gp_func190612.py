import csv
from collections import OrderedDict
from .definition import *
import re


# global
# enzymes = {"N": [], "O": [], "L": [], "G": [], "none": []}
# glystr = {"N": OrderedDict(), "O": OrderedDict(), "L": OrderedDict(), "G": OrderedDict(), "none": OrderedDict()}
# glytree = {"N": [], "O": [], "L": [], "G": [], "none": []}
# gtypes = ["N", "O", "L", "G", "none"]

#MS linkage list


def init_dict(gtypes: list):
    glystr = {}
    glytree = {}
    enzymes = {}

    for gtype in gtypes:
        glystr[gtype] = OrderedDict()
        glytree[gtype] = OrderedDict()
        enzymes[gtype] = []

    return glystr, glytree, enzymes


# read enzyme info from new_enzyme.txt
def read_enzyme(enzymes: dict, info_enzymes: list):
    enzyme_name = ""
    enzyme = Enzyme()

    # info_enzymes = read_file(text_filename)

    def set_regex(string: str):
        nstr = ""
        string_list = ""
        y = ""
# ------------------------------escepe------------------------------
        if "none" in string:
            string = string.replace("none", "")
        #orで分割してリストにする→要素を一つずつ正規表現に置き換える
        string = string.split("or")
        for ns in range(len(string)):
            if info_enzyme[0] == "Substrate" or info_enzyme[0] == "Constraint":
                if "(" in string[ns]:
                    string[ns] = string[ns].replace("(", "\(")
                if ")" in string[ns]:
                    string[ns] = string[ns].replace(")", "\)")
            if ">" in string[ns]:
                string[ns] = string[ns].replace(">", "\>")
            if "<" in string[ns]:
                string[ns] = string[ns].replace("<", "\<")
            if "≧" in string[ns]:
                string[ns] = string[ns].replace("≧", "\≧")
            if "≦" in string[ns]:
                string[ns] = string.replace("≦", "\≦")
            if "=" in string[ns]:
                string[ns] = string[ns].replace("=", "\=")
            if "^" in string[ns]:
                string[ns] = string[ns].replace("^", "\^")
            if "~" in string[ns]:
                string[ns] = string[ns].replace("~", "\~")
            if "?" in string[ns]:
                string[ns] = string.replace("?", "\?")
            # if "@" in string[ns]:
            #     string[ns] = string[ns].replace("@", "")
            if "?" in string[ns]:
                string[ns] = string[ns].replace("?", ".")
            if "*" in info_enzyme[1]:
                string[ns] = string[ns].replace("*", "SU")
            if "..." in info_enzyme[1]:
                string[ns] = string[ns].replace("...", "…")

    # ---------------Productの(?P=...)の中身はsubstrateに(?P<...>(...)の中身を入れるようにする------------------
            if "…" in string[ns]:
                if info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("…", "\\g<ligand>",1)
                    string[ns] = string[ns].replace("…", "\\g<ligands>", 1)
                    string[ns] = string[ns].replace("…", "\\g<ligandss>", 1)
                elif info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("…", "(?P<ligand>((\(.*\))?(SU)?)*)",1)
                    string[ns] = string[ns].replace("…", "(?P<ligands>((\(.*\))?(SU)?)*)", 1)
                    string[ns] = string[ns].replace("…", "(?P<ligandss>((\(.*\))?(SU)?)*)", 1)
                else:
                    string[ns] = string[ns].replace("…", "((\(.*\))?(SU)?)*")
            if "_" in string[ns]:
                if info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("_", "\\g<continue>",1)
                    string[ns] = string[ns].replace("_", "\\g<continues>", 1)
                    string[ns] = string[ns].replace("_", "\\g<continuess>", 1)
                elif info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("_", "(?P<continue>((\(?.*\))?(SU)?)*)",1)
                    string[ns] = string[ns].replace("_", "(?P<continues>((\(?.*\))?(SU)?)*)", 1)
                    string[ns] = string[ns].replace("_", "(?P<continuess>((\(?.*\))?(SU)?)*)", 1)
                else:
                    string[ns] = string[ns].replace("_", "((\(?.*\))?(SU)?)*")
            if "+" in string[ns]:
                if info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("+", "\\g<branch>",1)
                    string[ns] = string[ns].replace("+", "\\g<branch>", 1)
                    string[ns] = string[ns].replace("+", "\\g<branch>", 1)
                elif info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("+", "(?P<branch>((\(?.*\))*))",1)
                    string[ns] = string[ns].replace("+", "(?P<branchs>((\(?.*\))*))", 1)
                    string[ns] = string[ns].replace("+", "(?P<branchss>((\(?.*\))*))", 1)
                else:
                    string[ns] = string[ns].replace("+", "(\(?.*\))*")

            if "$" in info_enzyme[1]:
                if info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("$", "\\g<modification>")
                    string[ns] = string[ns].replace("$", "\\g<modifications>")
                    string[ns] = string[ns].replace("$", "\\g<modificationss>")
                elif info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("$", "(?P<modification>(\[.+\])*)")
                    string[ns] = string[ns].replace("$", "(?P<modifications>(\[.+\])*)")
                    string[ns] = string[ns].replace("$", "(?P<modificationss>(\[.+\])*)")
                else:
                    string[ns] = string[ns].replace("$", "(\[.+\])*")
            if " " in string[ns]:
                string[ns] = string[ns].replace(" ", "")
            if "SU" in string[ns]:
                string[ns] = string[ns].replace("SU", "[A-Z]{1,2}(\[.+\])?[a|b][1-8]")
            if "!" in string[ns]:
                if info_enzyme[0] == "Constraint":
                    string[ns] = string[ns].replace("!", "")
                    string[ns] = "^(?!.*" + string[ns] + ").+$"
        # orでsplitされていた場合(~~|~~)で括って文字列に変換
        if ns > 0:
            for x in string:
                y += x
                y += "|"
            string = "(" + y.rstrip("|") + ")"
        # orがなかった場合、リストを文字列に変換する
        else:
            for x in string:
                y += x
            string = y
        return string

    

    for info_enzyme in info_enzymes:
        info_enzyme = info_enzyme.rstrip().split(" : ")
        if info_enzyme[0] == "Enzyme":
            enzyme_name = info_enzyme[1]
            enzyme.set_name(enzyme_name)
        elif info_enzyme[0] == "Class":
            enzyme.set_Class(info_enzyme[1])
        elif info_enzyme[0] == "Substrate":
            sub = set_regex(info_enzyme[1])
            enzyme.set_substrate(sub)
        elif info_enzyme[0] == "Product":
            pro = set_regex(info_enzyme[1])
            enzyme.set_product(pro)
        elif info_enzyme[0] == "Constraint":
            con = info_enzyme[1].split("&")
            for i in range(0, len(con)):
                con[i] = set_regex(con[i])
            enzyme.set_constraint(con)
        elif info_enzyme[0] == "Type":
            enzyme.set_Gtype(info_enzyme[1])
        elif info_enzyme[0] == "Cosubstrate":
            enzyme.set_cosubstrate(info_enzyme[1])
        elif info_enzyme[0] == "/":
            if enzyme_name != "":
                enzymes[enzyme.Gtype].append(enzyme)
                enzyme = Enzyme()

    return enzymes

    # read enzyme info from new_enzyme.txt
def reverse_read_enzyme(enzymes: dict, info_enzymes: list):
    enzyme_name = ""
    enzyme = Enzyme()

    # info_enzymes = read_file(text_filename)
    def set_regex(string: str):
        nstr = ""
        string_list = ""
        y = ""
# ------------------------------escepe------------------------------
        if "none" in string:
            string = string.replace("none", "")
        #orで分割してリストにする→要素を一つずつ正規表現に置き換える
        string = string.split("or")
        for ns in range(len(string)):
            if info_enzyme[0] == "Product" or info_enzyme[0] == "Constraint":
                if "(" in string[ns]:
                    string[ns] = string[ns].replace("(", "\(")
                if ")" in string[ns]:
                    string[ns] = string[ns].replace(")", "\)")
            if ">" in string[ns]:
                string[ns] = string[ns].replace(">", "\>")
            if "<" in string[ns]:
                string[ns] = string[ns].replace("<", "\<")
            if "≧" in string[ns]:
                string[ns] = string[ns].replace("≧", "\≧")
            if "≦" in string[ns]:
                string[ns] = string.replace("≦", "\≦")
            if "=" in string[ns]:
                string[ns] = string[ns].replace("=", "\=")
            if "^" in string[ns]:
                string[ns] = string[ns].replace("^", "\^")
            if "~" in string[ns]:
                string[ns] = string[ns].replace("~", "\~")
            if "?" in string[ns]:
                string[ns] = string.replace("?", "\?")
            # if "@" in string[ns]:
            #     string[ns] = string[ns].replace("@", "")
            if "?" in string[ns]:
                string[ns] = string[ns].replace("?", ".")
            if "*" in info_enzyme[1]:
                string[ns] = string[ns].replace("*", "SU")
            if "..." in info_enzyme[1]:
                string[ns] = string[ns].replace("...", "…")

    # ---------------Productの(?P=...)の中身はsubstrateに(?P<...>(...)の中身を入れるようにする------------------
            if "…" in string[ns]:
                if info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("…", "\\g<ligand>",1)
                    string[ns] = string[ns].replace("…", "\\g<ligands>", 1)
                    string[ns] = string[ns].replace("…", "\\g<ligandss>", 1)
                elif info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("…", "(?P<ligand>((\(.*\))?(SU)?)*)",1)
                    string[ns] = string[ns].replace("…", "(?P<ligands>((\(.*\))?(SU)?)*)", 1)
                    string[ns] = string[ns].replace("…", "(?P<ligandss>((\(.*\))?(SU)?)*)", 1)
                else:
                    string[ns] = string[ns].replace("…", "((\(.*\))?(SU)?)*")
            if "_" in string[ns]:
                if info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("_", "\\g<continue>",1)
                    string[ns] = string[ns].replace("_", "\\g<continues>", 1)
                    string[ns] = string[ns].replace("_", "\\g<continuess>", 1)
                elif info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("_", "(?P<continue>((\(?.*\))?(SU)?)*)",1)
                    string[ns] = string[ns].replace("_", "(?P<continues>((\(?.*\))?(SU)?)*)", 1)
                    string[ns] = string[ns].replace("_", "(?P<continuess>((\(?.*\))?(SU)?)*)", 1)
                else:
                    string[ns] = string[ns].replace("_", "((\(?.*\))?(SU)?)*")
            if "+" in string[ns]:
                if info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("+", "\\g<branch>",1)
                    string[ns] = string[ns].replace("+", "\\g<branch>", 1)
                    string[ns] = string[ns].replace("+", "\\g<branch>", 1)
                elif info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("+", "(?P<branch>((\(?.*\))*))",1)
                    string[ns] = string[ns].replace("+", "(?P<branchs>((\(?.*\))*))", 1)
                    string[ns] = string[ns].replace("+", "(?P<branchss>((\(?.*\))*))", 1)
                else:
                    string[ns] = string[ns].replace("+", "(\(?.*\))*")

            if "$" in info_enzyme[1]:
                if info_enzyme[0] == "Substrate":
                    string[ns] = string[ns].replace("$", "\\g<modification>")
                    string[ns] = string[ns].replace("$", "\\g<modifications>")
                    string[ns] = string[ns].replace("$", "\\g<modificationss>")
                elif info_enzyme[0] == "Product":
                    string[ns] = string[ns].replace("$", "(?P<modification>(\[.+\])*)")
                    string[ns] = string[ns].replace("$", "(?P<modifications>(\[.+\])*)")
                    string[ns] = string[ns].replace("$", "(?P<modificationss>(\[.+\])*)")
                else:
                    string[ns] = string[ns].replace("$", "(\[.+\])*")
            if " " in string[ns]:
                string[ns] = string[ns].replace(" ", "")
            if "SU" in string[ns]:
                string[ns] = string[ns].replace("SU", "[A-Z]{1,2}(\[.+\])?[a|b][1-8]")

            if "n" in string[ns]:
                if info_enzyme[0] == "Constraint":
                    # orの条件が入った時の例外も処理できるように()|もspritにいれる
                    string[ns] = re.split("n",string[ns])
                    # pythonの内包処理でstringのリストから空白を削除
                    string[ns] = [a for a in string[ns] if a != '']
                    # 個数の制限を(...){~,~}で表記
                    for i in range(len(string[ns])):
                        if "\>\=" in string[ns][i]:
                            string_list = string[ns][i].split("\>\=")
                            string[ns][i] = ("(" + string_list[0] + ")" + "{" + string_list[1] + ",}")
                        if "\<\=" in string[ns][i]:
                            string_list = string[ns][i].split("\<\=")
                            string[ns][i] = ("("+string_list[0]+")"+"{,"+string_list[1]+"}")
                        if "\=" in string[ns][i]:
                            string_list = string[ns][i].split("\=")
                            string[ns][i] = ("("+string_list[0]+")"+"{"+string_list[1]+"}")
                        if "\>" in string[ns][i]:
                            string_list = string[ns][i].split("\>")
                            string_list[1] = int(string_list[1])
                            string_list[1] += 1
                            string_list[1] = str(string_list[1])
                            string[ns][i] = ("(" + string_list[0] + ")" + "{" + string_list[1] + ",}")
                        if "\<" in string[ns][i]:
                            string_list = string[ns][i].split("\<")
                            string_list[1] = int(string_list[1])
                            string_list[1] -= 1
                            string_list[1] = str(string_list[1])
                            string[ns][i] = ("("+string_list[0]+")"+"{,"+string_list[1]+"}")
                    for x in string[ns]:
                        nstr += x
                    string[ns] = nstr
            if "!" in string[ns]:
                if info_enzyme[0] == "Constraint":
                    string[ns] = string[ns].replace("!", "")
                    string[ns] = "^(?!.*" + string[ns] + ").+$"
    
        # orでsplitされていた場合(~~|~~)で括って文字列に変換
        if ns > 0:
            for x in string:
                y += x
                y += "|"
            string = "(" + y.rstrip("|") + ")"
        # orがなかった場合、リストを文字列に変換する
        else:
            for x in string:
                y += x
            string = y
        
        return string


    for info_enzyme in info_enzymes:
        info_enzyme = info_enzyme.rstrip().split(" : ")
        if info_enzyme[0] == "Enzyme":
            enzyme_name = info_enzyme[1]
            enzyme.set_name(enzyme_name)
        elif info_enzyme[0] == "Class":
            enzyme.set_Class(info_enzyme[1])
        elif info_enzyme[0] == "Substrate":
            sub = set_regex(info_enzyme[1])
            enzyme.set_substrate(sub)
        elif info_enzyme[0] == "Product":
            pro = set_regex(info_enzyme[1])
            enzyme.set_product(pro)
        elif info_enzyme[0] == "Constraint":
            # tentative pass
            pass
            con = info_enzyme[1].split("&")
            for i in range(0, len(con)):
                con[i] = set_regex(con[i])
            enzyme.set_constraint(con)
        elif info_enzyme[0] == "Type":
            enzyme.set_Gtype(info_enzyme[1])
        elif info_enzyme[0] == "Cosubstrate":
            enzyme.set_cosubstrate(info_enzyme[1])
        elif info_enzyme[0] == "/":
            if enzyme_name != "":
                enzymes[enzyme.Gtype].append(enzyme)
                enzyme = Enzyme()

    return enzymes


# read glycan info from init_glycan.csv and build glycan object
def read_glystr(glystr: dict, glycan_infos: list):
    # glycan_infos = read_file(csv_filename)

    for glycan_info in glycan_infos:
        glycan_info = glycan_info.rstrip().split(",")
        try:
            glycan = Glycan(glycan_info[1], int(glycan_info[2]))
        except IndexError and ValueError:
            print("IndexError: Please check format of glycan.")
        else:
            if len(glycan_info) == 4:
                glycan.set_gtype(glycan_info[3])
            store_glystr(glystr, glycan)
            
def read_glycan_profile(glystr: dict, glycan_infos: list):
    # glycan_infos = read_file(csv_filename)
    
    for glycan_info in glycan_infos:
        
        try:
            """ gtype がないデータは除外 s"""
            glycan = Glycan(glycan_info["name"], 0, gtype=glycan_info["gtype"])
        except IndexError and ValueError:
            print("IndexError: Please check format of glycan.")
        except KeyError:
            print("KeyError: Please check keys name or gtype.")
        else:
            store_glystr(glystr, glycan)
    return glystr
            
# store glycan object IF NOT exist in glystr
def store_glystr(glystr: dict, glycan: Glycan) -> Glycan:
    pre_glycan = glystr[glycan.Gtype].get(glycan.structure)
    if pre_glycan:
        del glycan
        return pre_glycan
    else:
        glystr[glycan.Gtype][glycan.structure] = glycan
        glycan.set_id(len(glystr[glycan.Gtype]))
        return glycan

# store reaction object IF NOT exist in glytree
def store_glytree(glytree: dict, rxn: Reaction) -> Reaction:
    gtype = rxn.child.Gtype
    if rxn in glytree[gtype].values():
        return glytree
        
    else:
        
        id = gtype + "R" + str(len(glytree[gtype]) + 1)
        rxn.index = (id)
        glytree[gtype][rxn.index] = rxn
        
    return glytree

# write glycan info to glystr.csv
def write_glystr(glystr: dict, csv_filename: str):
    try:
        with open(csv_filename, 'w') as fo:
            writer = csv.writer(fo, lineterminator='\n')
            for glystr in glystr.values():
                for str in glystr.values():
                    str_info = [str.id, str.structure, str.size, str.Gtype]
                    writer.writerow(str_info)
    except IOError:
        print("IOError: Cannot open file.")


# write tree info to glytree.csv
def write_glytree(glytree: dict, csv_filename: str):
    try:
        with open(csv_filename, 'w') as fo:
            writer = csv.writer(fo, lineterminator='\n')
            for glytree in glytree.values():
                for key, rxn in glytree.items():
                    # tree.insert(0, str(glytree.index(tree) + 1))
                    row = [
                        rxn.index,
                        rxn.child.id, 
                        rxn.parent.id,
                        rxn.enzyme.name,
                        rxn.enzyme.coproduct,
                        rxn.enzyme.cosubstrate,
                        rxn.enzyme.Class]

                    writer.writerow(row)
    except IOError:
        print("IOError: Cannot open file.")


# read_glystr("/Users/Sachiko/Dropbox/Simulation/Ver9/glystr_co.csv")
# write_glystr("/Users/Sachiko/Dropbox/Simulation/Ver9/glystr_newco.csv")
# read enzyme info from new_enzyme.txt

def glycan_set_regex(string: str):
    """Conver LC code to Regex expression."""
    nstr = ""
    string_list = ""
    y = ""


# ------------------------------escepe------------------------------
    if "none" in string:
        string = string.replace("none", "")
    #orで分割してリストにする→要素を一つずつ正規表現に置き換える
    string = string.split("or")
    for ns in range(len(string)):
    
        if "(" in string[ns]:
            string[ns] = string[ns].replace("(", "\(")
        if ")" in string[ns]:
            string[ns] = string[ns].replace(")", "\)")
        
        if "^" in string[ns]:
            string[ns] = string[ns].replace("^", "\^")
        if "~" in string[ns]:
            string[ns] = string[ns].replace("~", "\~")
        
        # if "@" in string[ns]:
        #     string[ns] = string[ns].replace("@", "")
        if "?" in string[ns]:
            string[ns] = string[ns].replace("?", "([1-9]|[a|b])")
        if "*" in string[ns]:
            string[ns] = string[ns].replace("*", "SU")
        if "..." in string[ns]:
            string[ns] = string[ns].replace("...", "…")
        if "//" in string[ns]:
            reg = re.compile(r"(?P<f>[A-Z]{1,2}(\[.+\])?[a|b][1-8])//(?P<b>[A-Z]{1,2}(\[.+\])?[a|b][1-8])")
            string[ns] = reg.sub(r"(\g<f>)|(\g<b>)",string[ns])

        if "/" in string[ns]:
            reg = re.compile(r"[1-8]/[1-8]")
            i = 0
            for m in reg.finditer(string[ns]):
                start = m.start() + i
                end = m.end() + i
                string[ns] = string[ns][:start] + "[" + string[ns][start] + "|" + string[ns][end-1] + "]" + string[ns][end:]
                i += 2

# ---------------Productの(?P=...)の中身はsubstrateに(?P<...>(...)の中身を入れるようにする------------------
        if "…" in string[ns]:
        
            string[ns] = string[ns].replace("…", "(?P<ligand>((\(.*\))?(SU)?)*)",1)
            string[ns] = string[ns].replace("…", "(?P<ligands>((\(.*\))?(SU)?)*)", 1)
            string[ns] = string[ns].replace("…", "(?P<ligandss>((\(.*\))?(SU)?)*)", 1)
        
        if "_" in string[ns]:
            
            string[ns] = string[ns].replace("_", "(?P<continue>((\(?.*\))?(SU)?)*)",1)
            string[ns] = string[ns].replace("_", "(?P<continues>((\(?.*\))?(SU)?)*)", 1)
            string[ns] = string[ns].replace("_", "(?P<continuess>((\(?.*\))?(SU)?)*)", 1)
            
        if "+" in string[ns]:
        
            string[ns] = string[ns].replace("+", "(?P<branch>((\(?.*\))*))",1)
            string[ns] = string[ns].replace("+", "(?P<branchs>((\(?.*\))*))", 1)
            string[ns] = string[ns].replace("+", "(?P<branchss>((\(?.*\))*))", 1)
            
        if "$" in string[ns]:
            
            string[ns] = string[ns].replace("$", "(?P<modification>(\[.+\])*)")
            string[ns] = string[ns].replace("$", "(?P<modifications>(\[.+\])*)")
            string[ns] = string[ns].replace("$", "(?P<modificationss>(\[.+\])*)")
            
        if " " in string[ns]:
            string[ns] = string[ns].replace(" ", "")
        if "SU" in string[ns]:
            string[ns] = string[ns].replace("SU", "[A-Z]{1,2}(\[.+\])?[a|b][1-8]")
        
    # orでsplitされていた場合(~~|~~)で括って文字列に変換
    if ns > 0:
        for x in string:
            y += x
            y += "|"
        string = "(" + y.rstrip("|") + ")"
    # orがなかった場合、リストを文字列に変換する
    else:
        for x in string:
            y += x
        string = y
    return string

