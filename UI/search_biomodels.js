
  
  
  ///////////////////////
  ///////////////////////
  // search BIOMODELS ///
  ///////////////////////
  ///////////////////////
function getmodelsbml(selected_modelid){
  $("#overlay").fadeIn();
  console.log('MODEL ID => ' + selected_modelid)
  jQuery(document).ready(function(){
    jQuery('#searchmodal').modal('hide');
  });
  let json = {
    'modelid': selected_modelid,
  }
  $.ajax({
      type: "POST",
      url: apiUrl + "/get_sbml_by_biomodels",
      contentType: "application/json",
      data: JSON.stringify(json),
      dataType: "json",
      scriptCharset: "utf-8",
  })
  .done(function(result) {
        setviewer(result)
  }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(textStatus);
      console.log(errorThrown);
  }).always(function() {
    $("#overlay").fadeOut();
  });
}


  function fgetmodelid(selected_modelid) {
    console.log(selected_modelid)
    getmodelsbml(selected_modelid)
  }

  
  
  document.getElementById("search_button").onclick = function() {
    $("#overlay").fadeIn();
    let json = {
      'query': $("#search").val(),
    }
    $.ajax({
        type: "POST",
        url: apiUrl + "/search_models",
        contentType: "application/json",
        data: JSON.stringify(json),
        dataType: "json",
        scriptCharset: "utf-8",
      })
      .done(function(result) {
        console.log("done /search_models")
        model_dict = result['models']
  
        text = ''
        model_count = Object.keys(model_dict).length
        for(var i=0; i<model_count; i++){
          if(model_dict[i]['format'] == 'SBML'){
            text = text + '<div class="alert alert-success" id="search_sbml_'+i
                  + '" role="alert" onclick="fgetmodelid('
                  + "'" + model_dict[i]['id'] + "'" + ')">'
                  + '<a>' + model_dict[i]['name'] + '</a>'
                  + '<hr>'
                  + '<span style="font-size:8pt; color: gray;">'
                  + 'ID:'
                  + model_dict[i]['id']
                  + ' | Submitter:'
                  + model_dict[i]['submitter']
                  + ' | Uploaded date:'
                  + model_dict[i]['submissionDate'].slice(0,10)
                  + ' | Last modifided date:'
                  + model_dict[i]['lastModified'].slice(0,10)
                  + '</span>'
                  + '</div>'
          }
        }
        document.getElementById("search_results").innerHTML = "";
        document.getElementById("search_results").insertAdjacentHTML('beforeend',text);
  
        jQuery(document).ready(function(){
          jQuery('#searchmodal').modal('show');
        });
  
        // getmodelid(model_dict,model_count)
  
      })
      .fail(function() {
        alert("error(/search_models)");
      })
      .always(function() {
        $("#overlay").fadeOut();
      });
  }
  
  
  
