// POST HTTP /ss_setdata



let json = {}

document.getElementById("toggle_content").hidden = true;
var sidebar = document.getElementById("sidebar");
sidebar.style.marginTop = '-10px'; 

(function() {
 window.addEventListener("load", function () {
   $('[data-toggle="popover"]').popover();
 });
})();

var file_dow = "false"
var dlbutton = document.getElementById("dl_sbml");

function setviewer(result){
    document.getElementById("toggle_content").hidden = false;
    dlbutton.disabled = false
    //////////////////////////////////
    // set data species parameters//
    //////////////////////////////////
    document.getElementById('species_list').innerHTML = "";
    document.getElementById('parameter_list').innerHTML = "";
    document.getElementById('boxtitle').innerHTML = "";

    species_dic = result[0];
    console.log(species_dic)
    parameter_dic = result[1];
    console.log(parameter_dic)
    reaction_dic = result[2];
    console.log(reaction_dic)
    compartment_dic = result[3];
    console.log(compartment_dic)
    cyjson = result[4];
    console.log(cyjson)
    sbml_text = result[5];
    model_dic = result[6];
    console.log(model_dic)
    
    species_count = Object.keys(species_dic).length
    parameters_count = Object.keys(parameter_dic).length

    ////////////////////////////
    // cytscapeでパスウェイを表示//
    ////////////////////////////
    signalingcyjson = {'nodes':[], 'edges':[]}
    glycosilationcyjson = {'nodes':[], 'edges':[]}
    for(cyee in cyjson){
      if(cyee != "glycosilation"){
        signalingcyjson['nodes'] = signalingcyjson['nodes'].concat(cyjson[cyee]['nodes'])
        signalingcyjson['edges'] = signalingcyjson['edges'].concat(cyjson[cyee]['edges'])
      }else{
        glycosilationcyjson['nodes'] = glycosilationcyjson['nodes'].concat(cyjson[cyee]['nodes'])
        glycosilationcyjson['edges'] = glycosilationcyjson['edges'].concat(cyjson[cyee]['edges'])
      }
    } 
    sbgnstylejs(signalingcyjson,"preset")
    cy.panzoom()
    layout_clicker = 0

    
    // set species
    num = 0
    for(species_key in species_dic){
                inputtext = '<input class="namelis" type="text" class="namelis" id="s_value'+num+'" size="16" maxlength="10" value="'+species_dic[species_key]["initialAmount"]+'">' + species_dic[species_key]['substanceUnits'][0]

                svgbtn = '<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="skyblue" class="bi bi-chevron-double-down" viewBox="0 0 16 16">'
                          + '<path fill-rule="evenodd" d="M1.646 6.646a.5.5 0 0 1 .708 0L8 12.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>'
                          + '<path fill-rule="evenodd" d="M1.646 2.646a.5.5 0 0 1 .708 0L8 8.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>'
                          + '</svg>'

                collapse =  '<div class="col-xl-1 col-lg-1">'
                        + '<button class="btn-ques front" type="button" data-toggle="collapse" data-target="#collapse_s_'+num+'" aria-expanded="false" aria-controls="collapse_s_'+num+'">'
                        + svgbtn
                        + '</button>'
                        + '</div>'
                        + '<div class="col-xl-7 col-lg-7 backobj">'
                        + '<div class="namelis" id="s_name'+num+'">' + species_dic[species_key]["name"] + '</div>'
                        + '</div>'
                        + '<div class="col-xl-4 col-lg-4">'
                        + inputtext
                        + '</div>'

                 collapsecontent = '<div class="col-xl-12 col-lg-12">'
                        + '<div class="collapse" id="collapse_s_'+num+'">'
                        + '<div class="card card-body">'
                        + '<div class="row fs-14 mx-2 backobj">name:'+species_dic[species_key]["name"]+'</div>'
                        // + '<div>metaid:'+species_dic[species_key]["metaid"]+'</div>'
                        + '<div class="row fs-14 mx-2 backobj">id:'+species_key+'</div>'
                        + '<div class="row fs-14 mx-2 backobj">type:'+species_dic[species_key]['layout']['type']+'</div>'
                        + '<div class="row fs-14 mx-2 backobj">compartment:'+compartment_dic[species_dic[species_key]["compartment"]]['name']+'</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>'

                text = '<div class="row">' + collapse + '</div>' + '<div class="row">' + collapsecontent + '</div>'

        document.getElementById('species_list').insertAdjacentHTML('beforeend',text);
        num += 1
    }
    // set parameter
    num = 0
    for(parameter_key in parameter_dic){
        inputtext = '<nobr>' + '<input class="namelis" type="text" class="namelis" id="p_value'+num+'" size="16" maxlength="10" value="'+parameter_dic[parameter_key]["value"]+'">' + parameter_dic[parameter_key]['units'][0] + '</nobr>'
        svgbtn = '<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="skyblue" class="bi bi-chevron-double-down" viewBox="0 0 16 16">'
                  + '<path fill-rule="evenodd" d="M1.646 6.646a.5.5 0 0 1 .708 0L8 12.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>'
                  + '<path fill-rule="evenodd" d="M1.646 2.646a.5.5 0 0 1 .708 0L8 8.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>'
                  + '</svg>'
        collapse =  '<div class="col-xl-1 col-lg-1">'
                + '<button class="btn-ques front" type="button" data-toggle="collapse" data-target="#collapse_p_'+num+'" aria-expanded="false" aria-controls="collapse_p_'+num+'">'
                + svgbtn
                + '</button>'
                + '</div>'
                + '<div class="col-xl-7 col-lg-7 backobj">'
                + '<div class="namelis" id="p_name'+num+'">' + parameter_key + '</div>'
                + '</div>'
                + '<div class="col-xl-4 col-lg-4">'
                + inputtext
                + '</div>'
         collapsecontent = '<div class="col-xl-12 col-lg-12">'
                + '<div class="collapse" id="collapse_p_'+num+'">'
                + '<div class="card card-body">'
                + '<div class="row fs-14 mx-2 backobj">id:'+parameter_key+'</div>'
                + '<div class="row fs-14 mx-2 backobj">unit:'+parameter_dic[parameter_key]['units'][0]+'</div>'
                + '</div>'
                + '</div>'
                + '</div>'
        text = '<div class="row">' + collapse + '</div>' + '<div class="row">' + collapsecontent + '</div>'

        document.getElementById('parameter_list').insertAdjacentHTML('beforeend',text);
        num += 1
    }
    // set duration
    document.getElementById('durationtitle').innerHTML = "";
    document.getElementById('durationtitle').insertAdjacentHTML('beforeend','duration['+model_dic['timeUnits'][0]+']');
}

function get_data(sbml, datatype){
    let json = {'sbml': sbml, 'type': datatype}
    $.ajax({
        type: "POST",
        url: apiUrl + "/ss_setdata",
        contentType: "application/json",
        data: JSON.stringify(json),
        dataType: "json",
        scriptCharset: "utf-8",
    })
    .done(function(result){
        setviewer(result)
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(textStatus);
      console.log(errorThrown);
    });
}


function create_dataset(file_dow) {
      let json = {
        'duration': $("#duration").val(),
        'step': $("#step").val(),
        'sbml_text': sbml_text,
      }
      $.ajax({
          type: "POST",
          url: apiUrl + "/ss_writejson",
          contentType: "application/json",
          data: JSON.stringify(json),
          dataType: "json",
          scriptCharset: "utf-8",
        })
        .done(function(result) {
          console.log("done /ss_writejson")
          edit_sbmlpara(file_dow)
        })
        .fail(function(jqXHR, textStatus, errorThrown){
          alert("error(/ss_writejson)\n"+errorThrown);
        })
        .always(function() {
          $("#overlay").fadeOut();
        });
}


// create sbml file
function edit_sbmlpara(file_dow){
      set_species_dic = {}
      set_parameter_dic = {}
      num = 0
      for(species_key in species_dic){
        set_species_dic[species_key] = document.getElementById("s_value"+num).value
        num += 1
      }
      num = 0
      for(parameter_key in parameter_dic){
        set_parameter_dic[parameter_key] = document.getElementById("p_value"+num).value
        num += 1
      }


      let json = {
        "set_species_dic" : set_species_dic,
         "set_parameter_dic" : set_parameter_dic,
         'sbml_text': sbml_text,
       }
       console.log("run /echo_sbml")
      $.ajax({
          type: "POST",
          url: apiUrl + "/echo_sbml",
          contentType: "application/json",
          data: JSON.stringify(json),
          dataType: "json",
          scriptCharset: "utf-8",
        })
        .done(function(result) {
            console.log("done /echo_sbml")
            if(file_dow == "true"){
              file_dow == "false"
              text = result
              file_doenload(text)
            }else{
              open_window()
            }
          })
          .fail(function() {
            alert("error(/echo_sbml)");
          })
          .always(function() {
            $("#overlay").fadeOut();
          });
}

function open_window(){
      console.log("run open_window")
      window.open(
        "/cyt_sig",
        "_blank",
        "menubar=0,width=1000,height=600,top=100,left=100")
      console.log("done open_window")
      // loading modal
      // jQuery(document).ready(function(){
      //   jQuery('.modal').modal('hide');
      // });
}


document.getElementById("s_simButton").onclick = function() {
    $("#overlay").fadeIn();
    console.log("run s_simButton")
    create_dataset()
    console.log("done s_simButton")
    $("#overlay").fadeOut();
}

document.getElementById("sds_button").onclick = function() {
    document.getElementById("toggle_content").hidden = false;
    dlbutton.disabled = false
    //////////////////////
    // get file name//////
    //////////////////////
    console.log("run sds_button")
    const template_file = document.s_tempfile.template_file;
    const num = template_file.selectedIndex;
    inputfile = template_file.options[num].value;
    console.log("set:"+inputfile)

    //////////////////////////////////
    // set data species parameters//
    //////////////////////////////////
    document.getElementById('species_list').innerHTML = "";
    document.getElementById('parameter_list').innerHTML = "";
    document.getElementById('boxtitle').innerHTML = "";
    get_data(inputfile, 'file')
    console.log("done sds_button")
}

function file_doenload(text){
  dlbutton.disabled = false
  var blob = new Blob(
    [text],
    { "type": "xml/plain" })
    let link = document.createElement('a')
    link.href = window.URL.createObjectURL(blob)
    link.download = document.getElementById("filename").value+'.xml'
    link.click() 
}

document.getElementById("dl_sbml").onclick = function() {
  dlbutton.disabled = true
  console.log("download:"+document.getElementById("filename").value+".xml")
  create_dataset("true")
}

// input sbml file
window.addEventListener('load', () => {
  const f = document.getElementById('sbmlfile1');
  f.addEventListener('change', evt => {
    let input = evt.target;
    if (input.files.length == 0) {
      console.log('No file selected');
      return;
    }
    const file = input.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      document.getElementById("toggle_content").hidden = false;
      dlbutton.disabled = false
      //////////////////////////////////
      // set data species parameters//
      //////////////////////////////////
      document.getElementById('species_list').innerHTML = "";
      document.getElementById('parameter_list').innerHTML = "";
      document.getElementById('boxtitle').innerHTML = "";
      get_data(reader.result, 'text')
      console.log("done file input")
    };
    reader.readAsText(file);
  });
});


////////////////
// csv file ///
///////////////
function get_json(jsontype){
  num = 0
  jQuery(document).ready(function(){
    jQuery('.modal').modal('hide');
  });
  if(jsontype == 'specieslist'){
    text = 'id,name,initialamount\n'
    for(species_key in species_dic){
      text = text + species_key + ',' + species_dic[species_key]['name'] + ',' + document.getElementById("s_value"+num).value + '\n' 
      num += 1
    }
  }
  if(jsontype == 'parameterslist'){
    text = 'id,value\n'
    for(parameter_key in parameter_dic){
      text = text + parameter_key + ',' + document.getElementById("p_value"+num).value + '\n'
      num += 1
    }
  }
  var blobj = new Blob(
    [text],
    { "type": "text/csv" })
    let link = document.createElement('a')
    link.href = window.URL.createObjectURL(blobj)
    link.download = jsontype + ".csv"
    link.click() 
}

window.addEventListener('load', () => {
  const f = document.getElementById('speciesjson');
  f.addEventListener('change', evt => {
    let input = evt.target;
    if (input.files.length == 0) {
      console.log('No file selected');
      return;
    }
    const file = input.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      // close modal
      jQuery(document).ready(function(){
        jQuery('.modal').modal('hide');
      });
      // inport csv
      csvdata = reader.result
      csvdata = csvdata.replaceAll('\n',',')
      csvdata = csvdata.split(',')
      num = 0
      for(species_key in species_dic){
        for(i=3; i<csvdata.length; i=i+3){
          if(csvdata[i] == species_key && csvdata[i+1] == species_dic[species_key]['name']){
            document.getElementById('s_value'+num).value = csvdata[i+2]
          }
        }
        num += 1
      }
    };
    reader.readAsText(file);
  });
});
window.addEventListener('load', () => {
  const f = document.getElementById('parametersjson');
  f.addEventListener('change', evt => {
    let input = evt.target;
    if (input.files.length == 0) {
      console.log('No file selected');
      return;
    }
    const file = input.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      // close modal
      jQuery(document).ready(function(){
        jQuery('.modal').modal('hide');
      });
      // inport csv
      csvdata = reader.result
      csvdata = csvdata.replaceAll('\n',',')
      csvdata = csvdata.split(',')
      num = 0
      for(parameter_key in parameter_dic){
        for(i=2; i<csvdata.length; i=i+2){
          if(csvdata[i] == parameter_key){
            document.getElementById('p_value'+num).value = csvdata[i+1]
          }
        }
        num += 1
      }
    };
    reader.readAsText(file);
  });
});
