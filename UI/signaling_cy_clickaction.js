function cy_clickevent(cy,cyjson){

    document.getElementById("cy-layout").onclick = function() {
      layout_clicker += 1
      console.log(layout_clicker%5)
      var layout_list=["grid","circle","concentric","breadthfirst"]
      var layout = cy.layout({
        name: layout_list[layout_clicker%4]
      });
      layout.run();
    }
  
  
    cy.on('click', 'node', function(evt){
      compartment_dic["default"] = {"name":"default"}
      parameters_count = Object.keys(parameter_dic).length
      for(var j = 0; j < parameters_count; j++){
        textid="p_name"+String(j)
        document.getElementById(textid).style.backgroundColor = "";
      }
      document.getElementById('clickedcnotent').innerHTML = "";
      if(compartment_dic[this.id().replace('a','')]){
        text = '<div class="font-weight-bold text-primary mt-3">COMPARTMENT NAME:</div>'
                +compartment_dic[this.id().replace('a','')]["name"]
        document.getElementById('clickedcnotent').insertAdjacentHTML('beforeend',text);
      }
      
      for(var i=0; i<cyjson["nodes"].length; i++){
        if(cyjson["nodes"][i]["data"]["id"] == this.id()){
          console.log(this.id())
          j=0
          for(key in species_dic){
            textid="s_name"+String(j)
            document.getElementById(textid).style.backgroundColor = "";
            if(this.id() == key){
              if(species_dic[key]["name"].indexOf('Complex_br_') != -1){
                text = '<div class="font-weight-bold text-primary mt-3">NAME:</div>' 
                for(var k=0; k<species_dic[key]["name"].split('/').length; k++){
                  if(k==0){
                    text = text  + species_dic[key]["name"].split('/')[k].replace('Complex_br_(','').replace(/_br_/g,'').replace(/_sub_/,'') + '<br>'
                  }else{
                    text = text  + species_dic[key]["name"].split('/')[k].replace(/_br_/g,'').replace(/_sub_/,'') + '<br>'
                  }
                }
                text = text.slice(0,-5) + '<br>';
              }else{
                text = '<div class="font-weight-bold text-primary mt-3">NAME:</div>' 
                        + '<span><a href="https://www.uniprot.org/uniprot/?query=' + species_dic[key]["name"]  + '&sort=score" target="_blank">'
                        + '<svg width="1.2em" height="1.5em" viewBox="0 0 16 16" class="bi bi-question-octagon-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">'
                        + '<path fill-rule="evenodd" d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM5.496 6.033a.237.237 0 0 1-.24-.247C5.35 4.091 6.737 3.5 8.005 3.5c1.396 0 2.672.73 2.672 2.24 0 1.08-.635 1.594-1.244 2.057-.737.559-1.01.768-1.01 1.486v.105a.25.25 0 0 1-.25.25h-.81a.25.25 0 0 1-.25-.246l-.004-.217c-.038-.927.495-1.498 1.168-1.987.59-.444.965-.736.965-1.371 0-.825-.628-1.168-1.314-1.168-.803 0-1.253.478-1.342 1.134-.018.137-.128.25-.266.25h-.825zm2.325 6.443c-.584 0-1.009-.394-1.009-.927 0-.552.425-.94 1.01-.94.609 0 1.028.388 1.028.94 0 .533-.42.927-1.029.927z"/>'
                        + '</svg>'
                        + '</a></span>'
                        + species_dic[key]["name"] 
                        + '<br>'
              }
              text = text + '<div class="font-weight-bold text-success mt-3">TYPE:</div>' + species_dic[key]["layout"]["type"] + '<br>'
                    +'<div class="font-weight-bold text-info mt-3">COMPARTMENT:</div>' + compartment_dic[species_dic[key]["compartment"]]["name"] + '<br>'
                    +'<div class="font-weight-bold text-warning mt-3">INITIALAMOUNT:</div>' + species_dic[key]["initialAmount"] + '<br>'
              document.getElementById('clickedcnotent').insertAdjacentHTML('beforeend',text);
              document.getElementById(textid).style.backgroundColor = "yellow";
              selected_species = key
    
  
              // add set modal
              text = '<br><button type="button" class="btn btn-outline-primary mt-3" data-toggle="modal" data-target="#setmodel">'
                      +'Set initial amount'
                      +'</button>'
                      +'<div class="modal fade" id="setmodel" tabindex="-1" role="dialog" aria-labelledby="setmodelLabel" aria-hidden="true">'
                      +'<div class="modal-dialog modal-lg" role="document">'
                      +'<div class="modal-content">'
                      +'<div class="modal-header">'
                      +'<h5 class="modal-title" id="setmodel">Set initial amount</h5>'
                      +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                      +'<span aria-hidden="true">&times;</span>'
                      +'</button>'
                      +'</div>'
                      +'<div class="modal-body">'
                      +'<br><div class="font-weight-bold text-danger mt-3">Initial amount:</div>'
              s_name=document.getElementById("s_name"+j).innerHTML
              text = text
                    +'<div class="row">'
                    +'<div class="col-xl-5 col-lg-5">'
                    +s_name
                    +'</div>'
              text = text
                    +'<div class="col-xl-3 col-lg-3">'
                    +species_dic[key]["initialAmount"]
                    +'</div>'
              text = text
                    +'<div class="col-xl-1 col-lg-1">'
                    +'=>'
                    +'</div>'
              s_value=document.getElementById("s_value"+j).value
              text = text
                    +'<div class="col-xl-3 col-lg-3">'
                    +'<input class="pathset" type="text" id="path_value'+j+'" size="16" maxlength="10"value="'+s_value+'">'
                    +'</div>'
                    +'</div>'
              text =  text+'</div>'
                      +'<div class="modal-footer">'
                      +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                      +'<button type="button" id="setbyedge" class="btn btn-primary" data-dismiss="modal">Save changes</button>'
                      +'</div>'
                      +'</div>'
                      +'</div>'
                      +'</div>'
              document.getElementById('clickedcnotent').insertAdjacentHTML('beforeend',text);
            }
            j+=1
          }
        }
      }
      try {
        document.getElementById("setbyedge").onclick = function() {
          console.log(document.getElementById("s_value"+selected_species).value)
          console.log('=>')
          s_value=document.getElementById("path_value"+selected_species).value
          console.log(s_value)
          console.log('---------------------')
          document.getElementById("s_value"+selected_species).value = document.getElementById("path_value"+selected_species).value
        }
      }
      catch(error){
          console.log("species none")
      }
    });

    cy.on('click', 'edge', function(evt){
      species_count = Object.keys(species_dic).length
      for (var j=0; j<species_count; j++){
        textid="s_name"+String(j)
        document.getElementById(textid).style.backgroundColor = "";
      }
      document.getElementById('clickedcnotent').innerHTML = "";
      for (var i=0; i<cyjson["edges"].length; ++i) {
        if(cyjson["edges"][i]["data"]["id"] == this.id()){
          console.log(this.id())
          for (let key in reaction_dic) {
            if(cyjson["edges"][i]["data"]["id"].split('-')[0] == key){
              text = '<div class="font-weight-bold text-primary mt-3">ID:</div>'
              text = text + key
              text = text + '<br><div class="font-weight-bold text-success mt-3">REACTANTS:</div>'
              for(var j=0; j<reaction_dic[key]['Reactant'].length; ++j){
                for(species_key in species_dic){
                  if(reaction_dic[key]['Reactant'][j] == species_key){
                    text = text +species_dic[species_key]["name"] + '<br>'
                  }
                }
              }
              text = text + '<br><div class="font-weight-bold text-info mt-3">PRODUCTS:</div>'
              for(var j=0; j<reaction_dic[key]['Product'].length; ++j){
                for(species_key in species_dic){
                  if(reaction_dic[key]['Product'][j] == species_key){
                    text = text +species_dic[species_key]["name"] + '<br>'
                  }
                }
              }
              text = text + '<br><div class="font-weight-bold text-warning mt-3">ENZYMES:</div>'
              for(var j=0; j<reaction_dic[key]['Modifiers'].length; ++j){
                for(species_key in species_dic){
                  if(reaction_dic[key]['Modifiers'][j] == species_key){
                    text = text +species_dic[species_key]["name"] + '<br>'
                  }
                }
              }
              clockedkey = key
              text = text+'<br><button type="button" id="kintic-btn" class="btn btn btn-outline-danger mt-3">Kinetic law</button>'
              document.getElementById('clickedcnotent').insertAdjacentHTML('beforeend',text);
  
            
              selected_paralist=[]
              j=0
              for(parameter_key in parameter_dic){
                textid="p_name"+String(j)
                document.getElementById(textid).style.backgroundColor = "";
                if(reaction_dic[key]['formula'].match(parameter_key)){
                  selected_paralist.push(j)
                  document.getElementById(textid).style.backgroundColor = "yellow";
                }
                j+=1
              }

              // for(species_key in species_dic){
                // console.log(reaction_dic[key]['kineticLaw'])
                // reaction_dic[key]['kineticLaw'] = reaction_dic[key]['kineticLaw'].replaceAll(species_key,species_dic[species_key]['name'])
              // }
  
              // add edit modal
              text = '<br><button type="button" class="btn btn-outline-primary mt-3" data-toggle="modal" data-target="#setmodel">'
                      +'Set parameters'
                      +'</button>'
                      +'<div class="modal fade" id="setmodel" tabindex="-1" role="dialog" aria-labelledby="setmodelLabel" aria-hidden="true">'
                      +'<div class="modal-dialog modal-lg" role="document">'
                      +'<div class="modal-content">'
                      +'<div class="modal-header">'
                      +'<h5 class="modal-title" id="setmodel">Set parameters</h5>'
                      +'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                      +'<span aria-hidden="true">&times;</span>'
                      +'</button>'
                      +'</div>'
                      +'<div class="modal-body">'
                      +'<iframe src="https://www.wiris.net/demo/editor/render?format=svg&mml='
                      +reaction_dic[key]['kineticLaw']
                      +'" width="100%" height="100"></iframe>'  
                      +'<br><div class="font-weight-bold text-danger mt-3">PARAMETERS:</div>'
                      x=0
                      for(parameter_key in parameter_dic){
                        if(x in selected_paralist){
                          p_name=document.getElementById("p_name"+selected_paralist[x]).innerHTML
                          text = text
                                +'<div class="row">'
                                +'<div class="col-xl-5 col-lg-5">'
                                +p_name
                                +'</div>'
                          text = text
                                +'<div class="col-xl-3 col-lg-3">'
                                +parameter_dic[parameter_key]["value"]
                                +'</div>'
                          text = text
                                +'<div class="col-xl-1 col-lg-1">'
                                +'=>'
                                +'</div>'
                          p_value=document.getElementById("p_value"+selected_paralist[x]).value
                          text = text
                                +'<div class="col-xl-3 col-lg-3">'
                                +'<input class="pathset" type="text" id="path_value'+x+'" size="16" maxlength="10"value="'+p_value+'">'
                                +'</div>'
                                +'</div>'
                        }
                        x+=1
                      }
              text =  text+'</div>'
                      +'<div class="modal-footer">'
                      +'<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'
                      +'<button type="button" id="setbynode" class="btn btn-primary" data-dismiss="modal">Save changes</button>'
                      +'</div>'
                      +'</div>'
                      +'</div>'
                      +'</div>'
              document.getElementById('clickedcnotent').insertAdjacentHTML('beforeend',text);
            }
          }
        }
      }
      try {
        document.getElementById("setbynode").onclick = function() {
          console.log('changed')
          for(var x = 0; x < selected_paralist.length; x++){
            console.log(document.getElementById("p_value"+selected_paralist[x]).value)
            console.log('=>')
            p_value=document.getElementById("path_value"+x).value
            console.log(p_value)
            console.log('---------------------')
            document.getElementById("p_value"+selected_paralist[x]).value = document.getElementById("path_value"+x).value
          }
        }
      } catch(error){
          console.log("parameter is none")
      }
      document.getElementById("kintic-btn").onclick = function() {
        kinetic_url = 'https://www.wiris.net/demo/editor/render?format=svg&mml='+reaction_dic[clockedkey]['kineticLaw']
        window.open(kinetic_url, null, 'width=700, height=400, toolbar=yes,menubar=yes,scrollbars=yes');
      }
    }); 
  }
  
  
  
