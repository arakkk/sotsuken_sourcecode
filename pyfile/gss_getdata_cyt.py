##################################################################
#cytoscapeとhtmlで使用するためのSBMLファイルのデータをファイルから読み込み#
##################################################################
import re
from source.GPP import gp_main190612 as GPP
from source.SBML import copasi_timecourse
from os.path import abspath
import csv
from libsbml import *
import sys
import os.path
import libsbml
from xml.etree import ElementTree
import json
from source.GSS import SBML_setup
from source.GSS import editcytjs
from source.GSS import units2math


def sbml2cy(species_dic,parameter_dic,compartment_dic,reaction_dic):
    #####create cyjson#############
    cyjson = {}
    cyjson["nodes"] = []
    cyjson["edges"] = []
    # speciesの数によって表示の倍率を変更する
    mag = 1 + len(species_dic)/80
    ################################
    ####create species node#########
    ################################
    for key in species_dic:
        data = {}
        data["id"] = key
        if(species_dic[key]["layout"]["type"] == "PROTEIN"):
            data["class"] = "macromolecule"
        elif(species_dic[key]["layout"]["type"] == "SIMPLE_MOLECULE"):
            data["class"] = "simple chemical"
        elif(species_dic[key]["layout"]["type"] == "DEGRADED"):
            data["class"] = "source and sink"
        elif(species_dic[key]["layout"]["type"] == "DRUG"):
            data["class"] = "simple chemical"
        elif(species_dic[key]["layout"]["type"] == "GENE"):
            data["class"] = "nucleic acid feature"
        else:
            data["class"] = "phenotype"
        # data["class"] = "macromolecule"
        ############shape########################
        # 小さい丸
        #species_type = "unspecified entity"
        # ６角形
        #species_type = "phenotype"
        # 複合体
        #species_type = "complex"
        # 丸に/
        #species_type = "source and sink"
        #########################################
        try:
          data["label"] = species_dic[key]["name"]
        except:
          data["label"] = key.replace('_br_','').replace('_sub_','')
        data["stateVariables"] = []
        data["parent"] = species_dic[key]["compartment"]
        # data["clonemarker"] = "false"
        data["unitsOfInformation"] = []
        cyjson["nodes"].append({"data":data, "position": {"x": species_dic[key]["layout"]["x"]*mag, "y": species_dic[key]["layout"]["y"]*mag}})

    ################################
    ####create compartment node#####
    ################################
    for key in compartment_dic:
      if compartment_dic[key]["name"] != 'default':
        data = {}
        data["id"] = key
        data["class"] = "compartment"
        data["label"] = compartment_dic[key]["name"]
        data["stateVariables"] = []
        data["parent"] = ""
        # data["clonemarker"] = "false"
        data["unitsOfInformation"] = []
        cyjson["nodes"].append({"data":data, "position": {"x": compartment_dic[key]["layout"]["x"]*mag, "y": compartment_dic[key]["layout"]["y"]*mag}})

    ################################
    ####create edge#################
    ################################
    for key in reaction_dic:
        position_x = []
        position_y = []
        # レアクションの作成
        for s in range(len(reaction_dic[key]["Reactant"])):
            data = {}
            data["id"] = key+"-react"+str(s)
            data["class"] = "consumption"
            data["bendPointPositions"] = []
            data["cardinality"] = 0
            data["source"] = reaction_dic[key]["Reactant"][s]
            data["target"] = key+"-point"
            data["portsource"] = reaction_dic[key]["Reactant"][s]
            data["porttarget"] = key+"-point"
            cyjson["edges"].append({"data":data})
            # 中間地点を調べるために
            for species_key in species_dic:
              if reaction_dic[key]["Reactant"][s] == species_key:
                position_x.append(species_dic[species_key]["layout"]["x"])
                position_y.append(species_dic[species_key]["layout"]["y"])

        for t in range(len(reaction_dic[key]["Product"])):
            data = {}
            data["id"] = key+"-product"+str(t)
            data["class"] = "production"
            data["bendPointPositions"] = []
            data["cardinality"] = 0
            data["source"] = key+"-point"
            data["target"] = reaction_dic[key]["Product"][t]
            data["portsource"] = key+"-point"
            data["porttarget"] = reaction_dic[key]["Product"][t]
            cyjson["edges"].append({"data":data})
            # 中間地点を調べるために
            for species_key in species_dic:
              if reaction_dic[key]["Product"][t] == species_key:
                position_x.append(species_dic[species_key]["layout"]["x"])
                position_y.append(species_dic[species_key]["layout"]["y"])

        for m in range(len(reaction_dic[key]["Modifiers"])):
            data = {}
            data["id"] = key+"-modify"+str(t)
            data["class"] = "catalysis"
            data["bendPointPositions"] = []
            data["cardinality"] = 0
            data["source"] = reaction_dic[key]["Modifiers"][m]
            data["target"] = key+"-point"
            data["portsource"] = reaction_dic[key]["Modifiers"][m]
            data["porttarget"] = key+"-point"
            # data["arrow"] = "circle"
            cyjson["edges"].append({"data":data})
        # 中間地点　□　の作成
        xx = 0
        yy = 0
        for p in range(len(position_x)):
          xx += position_x[p]
          x_count = p + 1
        for p in range(len(position_y)):
          yy += position_y[p]
          y_count = p + 1
        xx = xx/x_count
        yy = yy/y_count
        data = {}
        data["id"] = key+"-point"
        data["class"] = "process"
        data["label"] = ""
        data["stateVariables"] = []
        data["parent"] = ""
        # data["clonemarker"] = "false"
        data["unitsOfInformation"] = []
        cyjson["nodes"].append({"data":data, "position": {"x": xx*mag, "y": yy*mag}})

    return cyjson



def get_dataset(signaling_sbase):
  print("run getdataset")
  species_dic = {}
  parameter_dic = {}
  compartment_dic = {}
  reaction_dic = {}
  model_dic = {}
  signaling_sbase = SBML_setup.main(signaling_sbase)
  signaling_model = signaling_sbase.getModel()
  mplugin = (signaling_model.getPlugin("layout"))
  ###########################################################################
  #######################libSBMLで書き換えたい（KINETICとcelldesigner）#########  
  sbml_text = writeSBMLToString(signaling_sbase)
  sbml_text = sbml_text.replace('\n','')
  sbml_text = sbml_text.replace('\t','')
  sbml_text = re.sub('\>( )*\<', '><', sbml_text)
  sbml_text = sbml_text.replace('> ','>').replace(' <','<')
  root = ElementTree.fromstring(sbml_text)
  xmls=root.tag.split("}")[0]+"}"
  #namespaceの辞書を作成
  ns = {}
  ns_celldesigner=""
  ns_mathml=""
  NAMESPACE = re.findall(r'xmlns\:\w*\=\s?[\"\']\S+[\"\']', sbml_text)
  for i in range(len(NAMESPACE)):
    ns[NAMESPACE[i].replace('xmlns:','').split("=")[0]] = NAMESPACE[i].split('"')[1].split('"')[0]
    if "celldesigner" in NAMESPACE[i].split('"')[1].split('"')[0]:
      ns_celldesigner = NAMESPACE[i].replace('xmlns:','').split("=")[0]
    if "MathML" in NAMESPACE[i].split('"')[1].split('"')[0]:
      ns_mathml = NAMESPACE[i].replace('xmlns:','').split("=")[0]
  ###########################################################################
  ###########################################################################
  unitdifinition_dic = {}
  for i in range(0,signaling_model.getNumUnitDefinitions()):
      unitdifinition = signaling_model.getUnitDefinition(i)
      unitdifinition_dic[unitdifinition.getId()] = []
      for j in range(0,unitdifinition.getNumUnits()):
          unit = unitdifinition.getUnit(j)
          kind = UnitKind_toString(unit.getKind())
          unitdifinition_dic[unitdifinition.getId()].append({'kind':kind,
                                                             'exponent':unit.getExponent(),
                                                             'scale':unit.getScale(),
                                                             'multiplier':unit.getMultiplier()
                                                            })
  model_dic['substanceUnits'] = ['',{'kind':signaling_model.getSubstanceUnits(),'exponent':1,'scale':0,'multiplier':1}]
  model_dic['timeUnits'] = ['',{'kind':signaling_model.getTimeUnits(),'exponent':1,'scale':0,'multiplier':1}]
  model_dic['volumeUnits'] = ['',{'kind':signaling_model.getVolumeUnits(),'exponent':1,'scale':0,'multiplier':1}]
  model_dic['areaUnits'] = ['',{'kind':signaling_model.getAreaUnits(),'exponent':1,'scale':0,'multiplier':1}]
  model_dic['lengthUnits'] = ['',{'kind':signaling_model.getLengthUnits(),'exponent':1,'scale':0,'multiplier':1}]
  model_dic['extentUnits'] = ['',{'kind':signaling_model.getExtentUnits(),'exponent':1,'scale':0,'multiplier':1}]
  # print(unitdifinition_dic)
  for unitdifinition in unitdifinition_dic:
    for model_dif in model_dic:
      if unitdifinition == model_dic[model_dif][1]['kind']:
        model_dic[model_dif] = units2math.main(unitdifinition_dic[unitdifinition])
  for model_dif in model_dic:
    if model_dic[model_dif][0] == '':
      model_dic[model_dif] = units2math.main(model_dic[model_dif])
    
  # for modeldif in model_dic:
    # if UnitKind_isValidUnitKindString(model_dic[modeldif][0]['kind'],3,1):
      # pass
    # else:
      # model_dic[modeldif] = {}
    



  for i in range(0, signaling_model.getNumSpecies()):
    species = signaling_model.getSpecies(i)
    species_id = species.getId()
    species_dic[species_id] = {}
    species_dic[species_id]["name"] = species.getName()
    species_dic[species_id]["metaid"] = species.getMetaId()
    species_dic[species_id]["initialAmount"] = species.getInitialAmount()
    species_dic[species_id]["initialConcentration"] = species.getInitialConcentration()
    species_dic[species_id]["compartment"] = species.getCompartment()
    if species.getSubstanceUnits() in unitdifinition_dic:
      species_dic[species_id]["substanceUnits"] = units2math.main(unitdifinition_dic[species.getSubstanceUnits()])
    else:
      species_dic[species_id]["substanceUnits"] = units2math.main([{'kind':species.getSubstanceUnits(),'exponent':1,'scale':0,'multiplier':1}])
    x=y=w=h=0
    species_type = ""
    layoutid = "none"
    try:
      for i in range(0, mplugin.getNumLayouts()):
        layout = mplugin.getLayout(i)
        for j in range(0, layout.getNumSpeciesGlyphs()):
          speciesGlyph = layout.getSpeciesGlyph(j)
          if species_id == speciesGlyph.getSpeciesId():
            layoutid = layout.getId()
            bbox = speciesGlyph.getBoundingBox()
            if speciesGlyph.getId().split('_')[1] == "MOLECULE":
              species_type =  speciesGlyph.getId().split('_')[0] + '_' + speciesGlyph.getId().split('_')[1]
            else:
              species_type =  speciesGlyph.getId().split('_')[0]
            x = bbox.getX()
            y = bbox.getY()
            w = bbox.getWidth()
            h = bbox.getHeight()
    except:
      pass
    species_dic[species_id]["layout"] = {'layout':layoutid, "type":species_type, "w":w, "h":h, "x":x, "y":y}


  for i in range(0, signaling_model.getNumParameters()):
    parameter = signaling_model.getParameter(i)
    parameter_id = parameter.getId()
    parameter_dic[parameter_id] = {}
    parameter_dic[parameter_id]["metaid"] = parameter.getMetaId()
    parameter_dic[parameter_id]["units"] = parameter.getUnits()
    parameter_dic[parameter_id]["value"] = parameter.getValue()
    if parameter.getUnits() in unitdifinition_dic:
      parameter_dic[parameter_id]["units"] = units2math.main(unitdifinition_dic[parameter.getUnits()])
    else:
      parameter_dic[parameter_id]["units"] = units2math.main([{'kind':parameter.getUnits(),'exponent':1,'scale':0,'multiplier':1}])


  for i in range(0, signaling_model.getNumCompartments()):
    compartment = signaling_model.getCompartment(i)
    compartment_id = compartment.getId()
    compartment_dic[compartment_id] = {}
    compartment_dic[compartment_id]["metaid"] = compartment.getMetaId()
    compartment_dic[compartment_id]["name"] = compartment.getName()
    if compartment_dic[compartment_id]["name"] == '':
      compartment_dic[compartment_id]["name"] = compartment_id
    x=y=w=h=0
    try:
      for i in range(0, mplugin.getNumLayouts()):
        layout = mplugin.getLayout(i)
        layoutid = layout.getId()
        for j in range(0, layout.getNumCompartmentGlyphs()):
          getCompartmentGlyph = layout.getCompartmentGlyph(j)
          if compartment_id == getCompartmentGlyph.getCompartmentId():
            bbox = speciesGlyph.getBoundingBox()
            x = bbox.getX()
            y = bbox.getY()
            w = bbox.getWidth()
            h = bbox.getHeight()
    except:
      pass
    compartment_dic[compartment_id]["layout"] = {'layout':layoutid, "w":w, "h":h, "x":x, "y":y}


  for i in range(0, signaling_model.getNumReactions()):
    reaction = signaling_model.getReaction(i)
    reaction_dic[reaction.getId()] = {}
    reaction_dic[reaction.getId()]["Reactant"] = []
    for j in range(0, reaction.getNumReactants()):
      reactant = reaction.getReactant(j)
      species = reactant.getSpecies()
      reaction_dic[reaction.getId()]["Reactant"].append(species)
    reaction_dic[reaction.getId()]["Product"] = []
    for j in range(0, reaction.getNumProducts()):
      reactant = reaction.getProduct(j)
      species = reactant.getSpecies()
      reaction_dic[reaction.getId()]["Product"].append(species)
    reaction_dic[reaction.getId()]["Modifiers"] = []
    for j in range(0, reaction.getNumModifiers()):
      reactant = reaction.getModifier(j)
      species = reactant.getSpecies()
      reaction_dic[reaction.getId()]["Modifiers"].append(species)
    kinetic = reaction.getKineticLaw()
    Math = kinetic.getMath()
    formula = formulaToString(Math)
    reaction_dic[reaction.getId()]["formula"] = formula



  #sbml_textの整形
  sbml_text = sbml_text.replace('xmlns="http://www.w3.org/1998/Math/MathML"','')
  sbml_text = sbml_text.replace('type="integer"','')
  sbml_text = sbml_text.replace(ns_mathml+':','')
  # SBMLファイルを文字列で読み込んでMATHタグの中身の文字列を取り出す関数
  sbml_text_list = sbml_text.split('<math')
  for i in range (len(sbml_text_list)):
    sbml_text_list[i] = '<math'+sbml_text_list[i].split('</math>')[0]+'</math>'
  listcount = 1
  for key in reaction_dic:
    for spescies_key in species_dic:
      kinetic = sbml_text_list[listcount].replace('>'+spescies_key+'<','>'+species_dic[spescies_key]["name"]+'<')
    reaction_dic[key]['kineticLaw'] = kinetic
    listcount += 1
  # json.dumpsは返り値が文字列でないと行けないから
  cyjson = sbml2cy(species_dic,parameter_dic,compartment_dic,reaction_dic)
  cydic = editcytjs.editcyt(cyjson,species_dic,compartment_dic)
  # print(cyjson)
  datalist = [species_dic, parameter_dic,reaction_dic,compartment_dic,cydic,sbml_text,model_dic]
  return datalist 
