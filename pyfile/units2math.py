from libsbml import *

def main(units):
    math_up = ''
    math_down = ''
    # print(units)
    for unit in units:
        if type(unit) != type({}):
            if units[0] == "":
                continue
            else:
                return units
        # replace kind
        # print(unit)
        if unit['kind'] == 'mole':
            unit['math'] = 'M'
        elif unit['kind'] == 'litre' or unit['kind'] == 'liter':
            unit['math'] = 'L'
        elif unit['kind'] == 'metre' or unit['kind'] == 'meter':
            unit['math'] = 'm'
        elif unit['kind'] == 'gram':
            unit['math'] == 'g'
        elif unit['kind'] == 'second':
            unit['math'] = 'sec'
        else:
            unit['math'] = unit['kind']
        # add scale
        if unit['scale'] == 3:
            unit['math'] = 'k' + unit['math']
        elif unit['scale'] == 2:
            unit['math'] = 'h' + unit['math']
        elif unit['scale'] == 1:
            unit['math'] = 'da' + unit['math']
        elif unit['scale'] == 0:
            pass
        elif unit['scale'] == -1:
            unit['math'] = 'd' + unit['math']
        elif unit['scale'] == -2:
            unit['math'] = 'c' + unit['math']
        elif unit['scale'] == -3:
            unit['math'] = 'm' + unit['math']
        elif unit['scale'] == -6:
            unit['math'] = 'μ' + unit['math']
        elif unit['scale'] == -9:
            unit['math'] = 'n' + unit['math']
        elif unit['scale'] == -12:
            unit['math'] = 'p' + unit['math']
        elif unit['scale'] == -15:
            unit['math'] = 'f' + unit['math']
        else:
            unit['math'] = unit['math'] + '^' + str(unit['scale'] * 10)
        # add multiplier
        if unit['multiplier'] != 1:
            if unit['kind'] == 'second' and unit['multiplier'] == 60:
                unit['math'] = 'min'
            elif unit['kind'] == 'second' and unit['multiplier'] == 3600: 
                unit['math'] = 'h'
            else:
                unit['math'] = str(unit['multiplier']) + '･' + unit['math']
        # add ecponent
        if abs(unit['exponent']) > 2:
            unit['math'] = unit['math'] + '^' + str(abs(unit['exponent']))
        # create math
        if unit['exponent'] > 0:
            math_up = math_up + unit['math'] + '･'
        else:
            math_down = math_down + unit['math'] + '･'

    if math_up == '':
        math = '/' + math_down[:-1]
    elif math_down == '':
        math = math_up[:-1]
    else:
        math = math_up[:-1] + '/' + math_down[:-1]
    if units[0] == '':
        units[0] = math
    else:
        units.insert(0,math)

    return units