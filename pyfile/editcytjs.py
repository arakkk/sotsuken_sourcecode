import re

def editcyt(cyjson,species_dic,compartment_dic):
    default_id=cytosol_id=nucleus_id=golgi_id=cis_id=medial_id=trans_id=TGN_id=ER_id=''
    for cyelement in cyjson['nodes']:
        gongicomp = ['cis','trans','medial','TGN']
        mag = 1 + len(species_dic)/80

        # species
        try:
            if compartment_dic[cyelement['data']['parent']]['name'] in gongicomp:
                cyelement['data']['class'] = 'simple chemical'
        except:
            pass


        m = re.match(r'.*\_.*\;.*', cyelement['data']['label'])
        if m != None:
            cyelement['data']['clonemarker'] = 'false'
            cyelement['data']['class'] = 'macromolecule'

        # compartment
        if cyelement['data']['class'] == 'compartment':
            if cyelement['data']['label'] == '':
                cyelement['data']['label'] = cyelement['data']['id']
            compname = cyelement['data']['label']
            if re.match('default', compname,re.IGNORECASE) != None:
                default_id = cyelement['data']['id']
            if re.match('cytosol', compname,re.IGNORECASE) != None:
                cytosol_id = cyelement['data']['id']
            if re.match('nucleus', compname,re.IGNORECASE) != None:
                nucleus_id = cyelement['data']['id']
            if re.match('golgi', compname,re.IGNORECASE) != None:
                golgi_id = cyelement['data']['id']
            if re.match('cis', compname,re.IGNORECASE) != None:
                cis_id = cyelement['data']['id']
            if re.match('medial', compname,re.IGNORECASE) != None:
                medial_id = cyelement['data']['id']
            if re.match('trans', compname,re.IGNORECASE) != None:
                trans_id = cyelement['data']['id']
            if re.match('TGN', compname,re.IGNORECASE) != None:
                TGN_id = cyelement['data']['id']
            if re.match('endoplasmic', compname,re.IGNORECASE) != None:
                ER_id = cyelement['data']['id']
            elif compname == 'ER':
                ER_id = cyelement['data']['id']
    
    # print(cytosol_id)
    # print(ER_id)

    for cyelement in cyjson['nodes']:
        if cyelement['data']['class'] == 'compartment':
            try:
                if default_id != '':
                    cyelement['data']['parent'] = default_id
                if cytosol_id != '':
                    if nucleus_id != '':
                        if cyelement['data']['id'] == nucleus_id:
                            cyelement['data']['parent'] = cytosol_id
                    if golgi_id != '':
                        if cyelement['data']['id'] == golgi_id:
                            cyelement['data']['parent'] = cytosol_id
                    if cis_id != '':
                        if cyelement['data']['id'] == cis_id:
                            cyelement['data']['parent'] = cytosol_id
                    if medial_id != '':
                        if cyelement['data']['id'] == medial_id:
                            cyelement['data']['parent'] = cytosol_id
                    if trans_id != '':
                        if cyelement['data']['id'] == trans_id:
                            cyelement['data']['parent'] = cytosol_id
                    if TGN_id != '':
                        if cyelement['data']['id'] == TGN_id:
                            cyelement['data']['parent'] = cytosol_id
                    if ER_id != '':
                        if cyelement['data']['id'] == ER_id:
                            cyelement['data']['parent'] = cytosol_id
            except:
                pass
    
    # 糖鎖合成とシグナリングのjsonを分けて作成→辞書に格納
    layoutlist = []
    cydic = {}
    nodelist = {}
    for key in species_dic:
        if species_dic[key]['layout']['layout'] not in layoutlist:
            layoutlist.append(species_dic[key]['layout']['layout'])
    for layoutid in layoutlist:
        print(layoutid)
        nodelist[layoutid] = []
        cydic[layoutid] = {}
        cydic[layoutid]['nodes'] = []
        cydic[layoutid]['edges'] = []
        for cyelement in cyjson['nodes']:
            try:
                if species_dic[cyelement['data']['id']]['layout']['layout'] == layoutid:
                    cydic[layoutid]['nodes'].append(cyelement)
                    nodelist[layoutid].append(cyelement['data']['id']) 
            except:
                cydic[layoutid]['nodes'].append(cyelement)

        for cyelement in cyjson['edges']:
            if cyelement['data']['source'] in nodelist[layoutid] or cyelement['data']['target'] in nodelist[layoutid]:
                cydic[layoutid]['edges'].append(cyelement)

        # 途切れたリアクションを消去
        removelist = []
        for node in cydic[layoutid]['nodes'][:]:
            judge = 0
            if '-point' in node['data']['id']:
                for edge in cydic[layoutid]['edges']:
                    if edge['data']['source'] == node['data']['id']:
                        judge += 1
                    if edge['data']['target'] == node['data']['id']:
                        judge += 1
                if judge < 2:
                    removelist.append(node['data']['id'])
                    cydic[layoutid]['nodes'].remove(node)
        for edge in cydic[layoutid]['edges'][:]:
            if edge['data']['source'] in removelist or edge['data']['target'] in removelist:
                cydic[layoutid]['edges'].remove(edge)
        # 使われてないコンパートメントを消去
        for node in cydic[layoutid]['nodes'][:]:
            if node['data']['class'] == 'compartment':
                judge = 0
                for subnode in cydic[layoutid]['nodes']:
                    if subnode['data']['class'] != 'compartment':
                        if subnode['data']['parent'] == node['data']['id']:
                            judge += 1
                if judge == 0:
                    cydic[layoutid]['nodes'].remove(node)
            else:
                pass


    inst = {}
    inst['glycosilation'] = cyjson
    return cydic
    # return inst
    
