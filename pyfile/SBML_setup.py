from libsbml import *
from xml.etree import ElementTree
import re

######################################################
############### celldesigner to layout ###############
###############v######################################
def celldesigner_layout_species(root,species):
  w=h=x=y=0
  species_type = ""
  xmlid = species.attrib["id"]
  try:
    species_type = species.find('.//{http://www.sbml.org/2001/ns/celldesigner}class').text        
    for species_layout in root.findall('.//{http://www.sbml.org/2001/ns/celldesigner}speciesAlias'):
      if(species.attrib["id"] == species_layout.attrib["species"]):
        w = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["w"]
        h = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["h"]
        x = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["x"]
        y = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["y"]
    for species_layout in root.findall('.//{http://www.sbml.org/2001/ns/celldesigner}complexSpeciesAlias'):
      if(species.attrib["id"] == species_layout.attrib["species"]):
        w = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["w"]
        h = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["h"]
        x = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["x"]
        y = species_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["y"]
  except:
    pass
    # print("cant get species position")
  return species_type,float(w),float(h),float(x),float(y),xmlid

def celldesigner_layout_compartment(root,compartment):
  w=h=x=y=0
  xmlid = compartment.attrib["id"]
  for compartment_layout in root.findall('.//{http://www.sbml.org/2001/ns/celldesigner}compartmentAlias'):
      if(compartment.attrib["id"] == compartment_layout.attrib["compartment"]):
          try:
            w = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["w"]
            h = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["h"]
            x = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["x"]
            y = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}bounds').attrib["y"]
          except:
            try:
              x = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}point').attrib["x"]
              y = compartment_layout.find('.//{http://www.sbml.org/2001/ns/celldesigner}point').attrib["y"]
            except:
              print("cant get compartment position")
  return float(w),float(h),float(x),float(y),xmlid

def celdesigner_to_layout(signaling_sbase):
  sbml_text = writeSBMLToString(signaling_sbase)
  sbml_text = sbml_text.replace('\n','')
  sbml_text = sbml_text.replace('\t','')
  sbml_text = re.sub('\>( )*\<', '><', sbml_text)
  sbml_text = sbml_text.replace('> ','>').replace(' <','<')
  # unitdifinitionの書き換え単位が非対応のものがたまにある
  sbml_text = sbml_text.replace('<unit kind="micro_mole" exponent="1" scale="0" multiplier="1"/>','<unit kind="mole" exponent="1" scale="-6" multiplier="1"/>')
  root = ElementTree.fromstring(sbml_text)
  xmls=root.tag.split("}")[0]+"}"
  #namespaceの辞書を作成
  ns = {}
  ns_celldesigner=""
  NAMESPACE = re.findall(r'xmlns\:\w*\=\s?[\"\']\S+[\"\']', sbml_text)
  for i in range(len(NAMESPACE)):
    ns[NAMESPACE[i].replace('xmlns:','').split("=")[0]] = NAMESPACE[i].split('"')[1].split('"')[0]
    if "celldesigner" in NAMESPACE[i].split('"')[1].split('"')[0]:
      ns_celldesigner = NAMESPACE[i].replace('xmlns:','').split("=")[0]
  if ns_celldesigner != "":
    #############################################
    ################ layoutの追加 ################
    #############################################
    setlayoutlist = []
    signaling_model = signaling_sbase.getModel()
    layoutns = LayoutPkgNamespaces(3, 1, 1)
    mplugin = (signaling_model.getPlugin("layout"))
    newlayout = ""
    for i in range(0, mplugin.getNumLayouts()):
      layout = mplugin.getLayout(i)
      if layout.getId() == "signaling":
        newlayout = layout
      for j in range(0, layout.getNumSpeciesGlyphs()):
        SpeciesGlyph = layout.getSpeciesGlyph(j)
        setlayoutlist.append(SpeciesGlyph.getSpeciesId())
      for j in range(0, layout.getNumCompartmentGlyphs()):
        CompartmentGlyph = layout.getCompartmentGlyph(j)
        setlayoutlist.append(CompartmentGlyph.getCompartmentId())    
    # setlayoutlist.append()
    if newlayout == "":
      newlayout = mplugin.createLayout()
      newlayout.setId("signaling")
    layout = newlayout

    # speciesの書き換え
    for species in root.findall('.//'+xmls+'species'):
      species_type,w,h,x,y,xmlid = celldesigner_layout_species(root,species)
      if xmlid not in setlayoutlist:
        speciesGlyph = layout.createSpeciesGlyph()
        speciesGlyph.setId(species_type+"_"+xmlid)
        speciesGlyph.setSpeciesId(xmlid)
        speciesGlyph.setBoundingBox(BoundingBox(layoutns, "boundingBox_"+xmlid, x, y, w, h))
    # compartmentの書き換え
    for compartment in root.findall('.//'+xmls+'compartment'):
      w,h,x,y,xmlid = celldesigner_layout_compartment(root,compartment)
      if xmlid not in setlayoutlist:
        compartmentGlyph = layout.createCompartmentGlyph()
        compartmentGlyph.setId("CompartmentGlyph_"+xmlid)
        compartmentGlyph.setCompartmentId(xmlid)
        compartmentGlyph.setBoundingBox(BoundingBox(layoutns, "boundingBox_"+xmlid, x, y, w, h))



#############################################
################ SBML2to3 ###################
#############################################
def convertDocToL3(doc):
  layoutNsUri = "http://www.sbml.org/sbml/level3/version1/layout/version1"
  layoutNs = LayoutPkgNamespaces(3, 1)
  renderNsUri = "http://www.sbml.org/sbml/level3/version1/render/version1"
  renderNs = RenderPkgNamespaces(3, 1)

  prop = ConversionProperties(SBMLNamespaces(3,1))
  prop.addOption('strict', False)
  prop.addOption('setLevelAndVersion', True)
  prop.addOption('ignorePackages', True)
  doc.convert(prop)


  docPlugin = doc.getPlugin("layout")
  if docPlugin != None:
    docPlugin.setElementNamespace(layoutNsUri)

  doc.getSBMLNamespaces().removePackageNamespace(3, 1, "layout", 1)
  doc.getSBMLNamespaces().addPackageNamespace("layout", 1)

  rdocPlugin = doc.getPlugin("render")
  if rdocPlugin!= None:
    rdocPlugin.setElementNamespace(renderNsUri)

  doc.getSBMLNamespaces().removePackageNamespace(3, 1, "render", 1)
  doc.getSBMLNamespaces().addPackageNamespace("render", 1)

  return doc

 
def convertFileToL3(inputFile, outputFile):
  doc  = readSBMLFromFile(inputFile)
  convertDocToL3(outputFile)

def convertFile(inputFile, outputFile):
  doc  = readSBMLFromFile(inputFile)
  if doc.getLevel() == 3:
    convertDocToL2(doc, outputFile)
  else:
    convertDocToL3(doc, outputFile)


def main(signaling_sbase):
  if signaling_sbase.getLevel() != 3:
    convertDocToL3(signaling_sbase)
  celdesigner_to_layout(signaling_sbase)  
  return signaling_sbase




filename = "./signaling.xml"
signaling_sbase = readSBML(filename)
signaling_model = signaling_sbase.getModel()
main(signaling_sbase)